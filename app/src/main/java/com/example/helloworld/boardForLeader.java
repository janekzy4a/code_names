package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class boardForLeader extends AppCompatActivity implements View.OnClickListener {


    private int nrOfYButtons =5;
    private int nrOfXButtons =5;

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    private int seed=100;
    private agentCard [][] cardsMatrix;
    private Button[][] buttons = new Button[nrOfYButtons][nrOfXButtons];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        boolean startColour = intent.getBooleanExtra("colour",true);
        try {
            seed = Integer.parseInt(message);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        cardsMatrix = new agentCard[nrOfYButtons][nrOfXButtons];
        randomization(startColour,seed);

        for (int i = 0; i < nrOfYButtons; i++) {
            for (int j = 0; j < nrOfXButtons; j++) {
                String buttonID = "button" + i + j;
                cardsMatrix[i][j].setResID(getResources().getIdentifier(buttonID, "id", getPackageName()));
                buttons[i][j] = findViewById(cardsMatrix[i][j].getResID());
                buttons[i][j].setOnClickListener(this);
                buttons[i][j].setText(cardsMatrix[i][j].getText());
                if (cardsMatrix[i][j].getColour()==0)
                    buttons[i][j].setBackgroundColor(Color.RED);
                else if (cardsMatrix[i][j].getColour()==1)
                    buttons[i][j].setBackgroundColor(Color.rgb(0,102,255));
                else if (cardsMatrix[i][j].getColour()==2)
                    buttons[i][j].setBackgroundColor(Color.GRAY);
                else if (cardsMatrix[i][j].getColour()==3)
                {
                    buttons[i][j].setBackgroundColor(Color.BLACK);
                    buttons[i][j].setTextColor(Color.WHITE);
                }
            }
        }
    }



    @Override
    public void onClick(View v) {
        for (int i = 0; i < nrOfYButtons; i++) {
            for (int j = 0; j < nrOfXButtons; j++) {
                if (v.getId()==cardsMatrix[i][j].getResID()){
                    if(cardsMatrix[i][j].isVisible()){
                        cardsMatrix[i][j].setVisible(false);
                        v.setBackgroundColor(Color.WHITE);

                    }

                    else{
                        cardsMatrix[i][j].setVisible(true);
                        if (cardsMatrix[i][j].getColour()==0)
                            v.setBackgroundColor(Color.RED);
                        else if (cardsMatrix[i][j].getColour()==1)
                            v.setBackgroundColor(Color.rgb(0,102,255));
                        else if (cardsMatrix[i][j].getColour()==2)
                            v.setBackgroundColor(Color.GRAY);
                        else if (cardsMatrix[i][j].getColour()==3)
                            v.setBackgroundColor(Color.BLACK);
                    }


                }}}
    }

    void randomization (boolean whoFirst, int seed) {

        Random random = new Random(seed);
        int [] colours;
        Integer [] textsrandom= new Integer[25];
        for (int i=0; i<25; i++) {
            textsrandom[i] = random.nextInt(listOfTexts.length);
                    }
        while(!areDistinct(textsrandom))
            for (int i=0; i<25; i++){
                textsrandom[i]=random.nextInt(listOfTexts.length);
        }

        if (whoFirst==true)
        {int [] redFirst= {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,3};
            colours= RandomizeArray(redFirst,seed);
        }
        else{
            int [] blueFirst= {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,3};
            colours= RandomizeArray(blueFirst,seed);
        }
        //List textList= getTexts("testText.csv");
        List<String> textList = Arrays.asList(listOfTexts);

        for (int i = 0; i < nrOfYButtons; i++) {
            for (int j = 0; j < nrOfXButtons; j++) {
                cardsMatrix[i][j]=new agentCard(colours[5*i+j], true,textList.get(textsrandom[5*i+j]).toString());

            }
        }
    }



    public static int[] RandomizeArray(int[] array, int seed){
        Random rgen = new Random(seed);  // Random number generator

        for (int i=0; i<array.length; i++) {
            int randomPosition = rgen.nextInt(array.length);
            int temp = array[i];
            array[i] = array[randomPosition];
            array[randomPosition] = temp;
        }

        return array;
    }

    public static boolean areDistinct(Integer[] arr)
    {
        // Put all array elements in a HashSet
        Set<Integer> s =
                new HashSet<Integer>(Arrays.asList(arr));

        // If all elements are distinct, size of
        // HashSet should be same array.
        return (s.size() == arr.length);
    }
    public static List getTexts (String fileName)
    {
        List records =new ArrayList();
        String line ;

        FileReader fileReader = null;
        try {
            fileReader = new FileReader(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        try {
            while(true)
            {

                if (!((line = bufferedReader.readLine()) != null))
                    break;

                records.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return records;
    }

    String[] listOfTexts={"kara", "tłok","pies","wiosna","smar"," telefon","kot","śledz","torba","zamek","radio","sowa","latarka","smok","wrózka","samolot","teczka","termos","skarbonka"," komputer",
            "zdjecie","zszywacz","lampa","monitor","globus","szkło","stal","jeż", "rama ","czapka ","52 karty","list", "portfel","kość"," grabie","długopis","kabel","plecak ","marchew","gruszka","pizza","naleśnik","ogórek","kiwi",
            "cyklop", "nimfa", "ogr", "hydra", "mysz", "pustynia", "las", "miotła", "nos" , "spodnie", "sosna" , "brzoza", "utopia" , "zbroja", "flaga", "biblioteka", "nosorożec", "ork", "pożar", "gitara", "więzienie", "lekarz", "dzik",
            "bohater", "koń", "kawior", "butla", "szanty", "anioł", "czarodziej", "statek" , "medal", "latawiec", "schabowy", "lipa", "linijka", "laser", "mróz", "próżnia" ," sznur"," życie"," szkło"," konar"," brzoskwinia"," namiot"," zapalniczka"," czepek"," wydra"," bóbr"," gąszcz"," pagórek"," mleko"," palec"," włosy"," teatr"," noc"," łóżko"," stół"," dywan"," miesiąc"," dom"," kakao"," bobas"," opera"," pidżama"," poduszka"," szary"," siwy"," fotel"," wiklina"," tętnica"," krew"," rower"," czółno"," storczyk"," muszelka"," dziura"," zapalniczka"," tarcza"," kalafior"," melon"," jagoda"," strzała"," dzida"," mecz"," zegar"," spodnie"," obraz"," laska"," ptaszek"," wiersz"," turbina"," farba"," deszcz"," drgawki"," szpital"," strażak"," rybak"," Jezus"," krzyż"," droga"," nitka"," igła"," strzykawka"," groszek"," cień"," skrzypce"," wrota"," wóz"," stodoła"," krowa"," ząb"," dentysta"," firanka"," tęcza"," most"," zegar"," malarz"," prezydent"," czołg"," miecz"," worek"," szerszeń"," skorpion"," patyk"," papryka"," rzeka"," mleko"," kolano"," łysina"," belka"," węgiel"," zderzak"," pole"," dwór"," Nowy Jork"," Belgia"," Meksyk"," Berlin"," Antarktyda"," Australia"," Kongo"," czapla"," Stambuł"," policjant"," lizak"," lód"," kanion"," guma"," atleta"," but"," marionetka"," wełna"," siodło"," pług"," ziarno"," handel"," tymianek"," oregano"," kadzidło"," czekan"," organy"," flet"," kasztan"," amor"," telefon"," list"," kochanek"," plama"," zioło"," kabriolet"," suknia"," smoking"," karuzela"," szydełko"," szkielet"," duch"," zwarcie"," lustro"," lutnia"," trójkąt"," kula"," choroba"," wypadek"," rzęsa"," koralowiec"," łosoś"," szlachcic"," supeł"," pomarańcz"," niebieski"," jelito"," kaszanka"," domino"," liść"," walizka"," dzban"," żołądź"," drut"," wafel"," wybrzeże"," skunks"," pleśń"," komar"," trociny"," paproć"," wizjer"," jarzębina"," serpentyna"," balon"," język"," wino"," segregator"," pończocha"," jajko"," kosz"," sito"," linijka"," tablica"," ucho"," waga"," wampir"," Indie"," mord"," habit"," astronauta"," Mars"," lew"," hieroglify"," lupa"," sonet"," klucz"," silnik"," Indianin"," śruba"," kalosze"," kombinerki"," opona"," miazga"," cerkiew"," dzwonek"," skrzat"," popiersie"," jaskinia"," statek"," ropa"," urząd"," meduza"," maska"," upał"," donica"," wieszak"," żyrandol"," Eskimos"," trzepak"," gilotyna"," liczydło"," parobek"," bibuła"," keczup"," zysk"," beton"," panna"," szyna"," szynka"," szakal"," emocje"," miniatura"," wilgoć"," szczęki"," rodzynka"," moralność"," trumna"," stołówka"," katapulta"," biceps"," kimono"," purpura"," wstążka"," ważka"," odkurzacz"," zawody"," klepsydra"," faraon"," komunizm"," plemię"," trubadur"," mężczyzna"," grypa"," pomnik"," dopływ"," kajak"," gogle"," ciśnienie"," depresja"," ciąg"," skupienie"," mandarynka"," demonstracja"," uczta"," lekcja"," antybiotyk"," listonosz"," życzenia"," golf"," negocjator"," gorset"," pościg"," kolczyki"," konfesjonał"," tabakierka"," monokl"," łańcuch"," frędzel"," szachy"," karczma"," sesja","  Rzym"," pierogi"," wachlarz"," pięta"," szampon"," Graal"," stos"," procesor"," uszczelka"," filtr"," wiatrak"," beret"," miś"," termometr"," kompas"," mąka"," Hong Kong","  gobelin"," talia"," figura"," srebro"};



}






package com.example.helloworld;

public class agentCard {

    public agentCard(int colour, boolean visible, String text) {
        this.colour = colour;
        this.visible = visible;
        this.text = text;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    private int colour;
    private boolean visible;
    private String text;

    public int getResID() {
        return ResID;
    }

    public void setResID(int resID) {
        ResID = resID;
    }

    private int ResID;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }






}

package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {
public  static  final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    public void playGame (View view)
{    Switch simpleSwitch = (Switch) findViewById(R.id.switchcolour);
    Boolean switchState = simpleSwitch.isChecked();
    Intent intent = new Intent(this, board.class);
    EditText editText = findViewById(R.id.editText2);
    String message = editText.getText().toString();
    intent.putExtra(EXTRA_MESSAGE, message);
    intent.putExtra("colour", switchState);
    startActivity(intent);}




    public void playGameAsLeader (View view)
    {   Switch simpleSwitch = (Switch) findViewById(R.id.switchcolour);
        Boolean switchState = simpleSwitch.isChecked();
        Intent intent = new Intent(this, boardForLeader.class);
        EditText editText = findViewById(R.id.editText2);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra("colour", switchState);
        startActivity(intent);}

}

